// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "CGameState.generated.h"


UENUM(BlueprintType)
enum class EGameState : uint8
{	
	GameInProgress,

	GameOver,

	Win, 

};

UCLASS()
class COINCOLLECTOR_API ACGameState : public AGameStateBase
{
	GENERATED_BODY()

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "GameState")
	void GameStateChanged(EGameState NewState, EGameState OldState);
	
	UPROPERTY(BlueprintReadOnly, Category = "GameState")
	EGameState GameState;
		
public:

	UFUNCTION()
	void SetWaveState(EGameState NewState);
	
};

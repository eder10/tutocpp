// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CWGTGameOver.generated.h"

/**
 * 
 */
UCLASS()
class COINCOLLECTOR_API UCWGTGameOver : public UUserWidget
{
	GENERATED_BODY()

public:

	UCWGTGameOver(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	// Method to update the text to show
	UFUNCTION()
	void UpdateText(FString ValueText);	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* TextGameOver = nullptr;
};

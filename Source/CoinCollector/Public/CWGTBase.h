// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CWGTBase.generated.h"

/**
 * 
 */
UCLASS()
class COINCOLLECTOR_API UCWGTBase : public UUserWidget
{
	GENERATED_BODY()

public:

	UCWGTBase(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;
	
	// Method to update the score to show
	UFUNCTION()
	void UpdateScore(int32 Value);

	UFUNCTION()
	void ResetScore();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* TextScore = nullptr;
	
};

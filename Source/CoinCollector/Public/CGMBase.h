// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CGMBase.generated.h"

enum class EGameState : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCoinCollected, AActor*, CoinActor, APawn*, BallPawn);

class ACPlayerState;
class ACHUDBase;
class ACPBasePlayer;

UCLASS()
class COINCOLLECTOR_API ACGMBase : public AGameModeBase
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	int32 NumberOfCoinsCollected = 0;

	UPROPERTY()
	int32 Seconds = 0;

	UPROPERTY()
	ACPBasePlayer* Player=nullptr;

	UPROPERTY()
	APlayerController* PlayerController=nullptr;	
			
public:

	ACGMBase();

	UPROPERTY()
	int32 CoinsEnemyCollected = 0;

	virtual void Tick(float DeltaSeconds) override;

	virtual void StartPlay() override;

	UFUNCTION()
	void IncrementSeconds();

	UFUNCTION()
	void FinalizeGame(FString TextValue);

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnCoinCollected OnCoinCollected;

	UFUNCTION(BlueprintCallable)
	TSubclassOf<APlayerState> GetPlayerStateCustom();

	UPROPERTY(BlueprintReadWrite, Category = "GameMode")
	ACHUDBase* CustomGameHud = nullptr;

	FTimerHandle SecondsTimerHandle;

	UFUNCTION()
	int32 GetSeconds();
};

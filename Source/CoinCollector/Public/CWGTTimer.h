// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CWGTTimer.generated.h"

/**
 * 
 */
UCLASS()
class COINCOLLECTOR_API UCWGTTimer : public UUserWidget
{
	GENERATED_BODY()

public:

	UCWGTTimer(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	FTimerHandle OneSecondTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* TextTimer = nullptr;

	// Method to update the time to show
	UFUNCTION()
	void UpdateTime(int32 Value);

	UFUNCTION()
	void ResetTime();
};

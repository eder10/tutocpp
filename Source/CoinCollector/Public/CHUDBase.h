// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "Components/WidgetComponent.h"
#include "CWGTBase.h"
#include "CWGTGameOver.h"
#include "CWGTTimer.h"

#include "CHUDBase.generated.h"

/**
 * 
 */
UCLASS()
class COINCOLLECTOR_API ACHUDBase : public AHUD
{
	GENERATED_BODY()

public:

	ACHUDBase();

	virtual void DrawHUD() override;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	void UpdateScoreText(int32 Value);

	UFUNCTION()
	void ResetScoreText();

	UFUNCTION()
	void UpdateTimerText(int32 Value);

	UFUNCTION()
	void ResetTimerText();

	UFUNCTION()
	void ShowGameOver(FString TextToShow);

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UUserWidget> ScoreWidgetClass = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UUserWidget> TimerWidgetClass = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Widgets")
	TSubclassOf<UUserWidget> GameOverWidgetClass = nullptr;
	
private:

	UPROPERTY()
	UCWGTBase* ScoreWidget = nullptr;

	UPROPERTY()
	UCWGTTimer* TimerWidget = nullptr;

	UPROPERTY()
	UCWGTGameOver* GameOverWidget = nullptr;
	
};

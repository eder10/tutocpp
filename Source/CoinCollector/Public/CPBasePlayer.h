// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CPBasePlayer.generated.h"

class UStaticMeshComponent;
class USpringArmComponent;
class UCameraComponent;
class UNiagaraSystem;

UCLASS()
class COINCOLLECTOR_API ACPBasePlayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACPBasePlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent * SpringArmComponent = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent * CameraComponent = nullptr;

	void MoveUp(float Value);

	void MoveRight(float Value);	
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Jump();

	UPROPERTY(EditAnywhere, Category = "FallingEffect")
	FLinearColor Color01 = FLinearColor(10.0f, 2.5f, 0.4f, 1.0f);

	UPROPERTY(EditAnywhere, Category = "FallingEffect")
	FLinearColor Color02 = FLinearColor(20.0f, 2.0f, 0.0f, 1.0f);
	
	UPROPERTY()
	bool bEffectPlayed = false;

	UPROPERTY()
	float PreviousZ = 1000.0f;

	UPROPERTY()
	float NoEffectDuration = 1.0f;

	FTimerHandle TimerHandle_NoPlayAreaEffect;

	UFUNCTION()
	void ReInitiateEffect();
	
private:
	// Niagara explotion
	UPROPERTY(EditDefaultsOnly, Category = "AreaEffect")
	class UNiagaraSystem * NiagaraExplotion = nullptr;

	UFUNCTION()
	void PlayFallingEffect();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float JumpImpulse = 100000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MovementForce = 100000.0f;

};

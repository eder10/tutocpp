// Fill out your copyright notice in the Description page of Project Settings.


#include "CHUDBase.h"

#include "CoinCollector.h"


void ACHUDBase::BeginPlay()
{
	Super::BeginPlay();

	if(ScoreWidgetClass)
	{
		ScoreWidget = CreateWidget<UCWGTBase>(GetWorld(),ScoreWidgetClass);

		if(ScoreWidget)
		{
			ScoreWidget->AddToViewport();
		}
	}

	if (TimerWidgetClass)
	{
		TimerWidget = CreateWidget<UCWGTTimer>(GetWorld(), TimerWidgetClass);

		if (TimerWidget)
		{
			TimerWidget->AddToViewport();
		}
	}

	if (GameOverWidgetClass)
	{
		GameOverWidget = CreateWidget<UCWGTGameOver>(GetWorld(), GameOverWidgetClass);

		if (GameOverWidget)
		{
			GameOverWidget->SetVisibility(ESlateVisibility::Hidden);
			GameOverWidget->AddToViewport();			
		}
	}
}

void ACHUDBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ACHUDBase::UpdateScoreText(int32 Value)
{
	if(ScoreWidget)
	{
		ScoreWidget->UpdateScore(Value);
	}
}

void ACHUDBase::ResetScoreText()
{
	if (ScoreWidget)
	{
		ScoreWidget->ResetScore();
	}
}

void ACHUDBase::UpdateTimerText(int32 Value)
{
	if (TimerWidget)
	{
		TimerWidget->UpdateTime(Value);
	}
}

void ACHUDBase::ResetTimerText()
{
	if (TimerWidget)
	{
		TimerWidget->ResetTime();
	}
}

void ACHUDBase::ShowGameOver(FString TextToShow)
{	 
	GameOverWidget->UpdateText(TextToShow);
	GameOverWidget->SetVisibility(ESlateVisibility::Visible);

	TimerWidget->SetVisibility(ESlateVisibility::Hidden);
	ScoreWidget->SetVisibility(ESlateVisibility::Hidden);
}


ACHUDBase::ACHUDBase()
{
}

void ACHUDBase::DrawHUD() 
{
	Super::DrawHUD();
}

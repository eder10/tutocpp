// Fill out your copyright notice in the Description page of Project Settings.


#include "CGMBase.h"
#include "CHUDBase.h"
#include "CGameState.h"
#include "CoinCollector.h"
#include "CPBasePlayer.h"
#include "CPlayerState.h"



ACGMBase::ACGMBase()
{	
	GameStateClass = ACGameState::StaticClass();
	PlayerStateClass = ACPlayerState::StaticClass();
}

void ACGMBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);	
}

void ACGMBase::StartPlay()
{
	Super::StartPlay();

	// Game HUD setting up
	CustomGameHud = Cast<ACHUDBase>(GetWorld()->GetFirstPlayerController()->GetHUD());

	//Setting up timer
	GetWorldTimerManager().SetTimer(SecondsTimerHandle, this, &ACGMBase::IncrementSeconds, 1.0f, true, 0.0f);

	Player = Cast<ACPBasePlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
	PlayerController = GetWorld()->GetFirstPlayerController();
}

void ACGMBase::IncrementSeconds()
{	
	// Lose the game because time left
	if (Seconds >= TIME_TO_LOSE)
	{
		FinalizeGame(GAMEOVER_LOSE_TEXT);
	}
	// Show actual Seconds value
	CustomGameHud->UpdateTimerText(Seconds);
	Seconds++;

	// Check if you already collected most of the coins
	if (Player->GetPlayerState()->Score > COINS_TO_WIN/2 && Player->GetPlayerState()->Score > CoinsEnemyCollected)
	{
		FinalizeGame(GAMEOVER_WIN_TEXT);
	}
}

void ACGMBase::FinalizeGame(FString TextValue)
{
	UE_LOG(LogTemp, Warning, TEXT("FINALIZE GAME!! "));
	// Show on HUD TEXT
	CustomGameHud->ShowGameOver(TextValue);
	
	// Show GameOver Widget
	GetWorldTimerManager().ClearTimer(SecondsTimerHandle);

	// Disable Controls	
	Player->DisableInput(PlayerController);		
	
}

TSubclassOf<APlayerState> ACGMBase::GetPlayerStateCustom()
{
	return PlayerStateClass;
}

int32 ACGMBase::GetSeconds()
{
	return Seconds;
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "CPlayerState.h"

void ACPlayerState::AddCoin(int32 CoinScore)
{
	Score += CoinScore;
}

int32 ACPlayerState::GetScore()
{
	return FMath::FloorToInt(Score);
	
}

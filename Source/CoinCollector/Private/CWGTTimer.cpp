// Fill out your copyright notice in the Description page of Project Settings.


#include "CWGTTimer.h"

#include "TextBlock.h"

UCWGTTimer::UCWGTTimer(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UCWGTTimer::NativeConstruct()
{
	Super::NativeConstruct();
	
}

void UCWGTTimer::UpdateTime(int32 Value)
{
	if (TextTimer)
	{
		TextTimer->SetVisibility(ESlateVisibility::Visible);
	}
	TextTimer->SetText(FText::FromString(FString::FromInt(Value)));
}

void UCWGTTimer::ResetTime()
{
	TextTimer->SetVisibility(ESlateVisibility::Hidden);
	TextTimer->SetText(FText::FromString(FString::FromInt(0)));
}

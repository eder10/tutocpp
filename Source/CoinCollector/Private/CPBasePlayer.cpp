// Fill out your copyright notice in the Description page of Project Settings.
#include "CPBasePlayer.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Niagara/Public/NiagaraComponent.h"
#include "Niagara/Public/NiagaraFunctionLibrary.h"

// Sets default values
ACPBasePlayer::ACPBasePlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComp");
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComp");

	RootComponent = MeshComponent;
	SpringArmComponent->SetupAttachment(MeshComponent);
	CameraComponent->SetupAttachment(SpringArmComponent);

	MeshComponent->SetSimulatePhysics(true);
	MovementForce = 100000.0f;

	// Bool for playing effect
	bEffectPlayed = false;
	NoEffectDuration = 1.0f;
	
}

// Called when the game starts or when spawned
void ACPBasePlayer::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACPBasePlayer::MoveUp(float Value)
{
	FVector ForceToAdd = FVector(1, 0, 0) * MovementForce * Value;
	MeshComponent->AddForce(ForceToAdd);
}

void ACPBasePlayer::MoveRight(float Value)
{
	FVector ForceToAdd = FVector(0, 1, 0) * MovementForce * Value;
	MeshComponent->AddForce(ForceToAdd);
}

void ACPBasePlayer::ReInitiateEffect()
{
	bEffectPlayed = false;
}

void ACPBasePlayer::PlayFallingEffect()
{
	// Manage Scale
	float RandomScale = FMath::RandRange(0.5f, 1.6f);

	// Manage Impact Delay
	float RandomDelay = FMath::RandRange(1.0f, 1.5f);	

	float LocationX = GetActorLocation().X;
	float LocationY = GetActorLocation().Y;
	UNiagaraComponent * NiagaraComp = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NiagaraExplotion, FVector(LocationX, LocationY, 30.0f), FRotator::ZeroRotator);

	NiagaraComp->SetFloatParameter(TEXT("Scale"), RandomScale);
	NiagaraComp->SetFloatParameter(TEXT("ImpactDelay"), RandomDelay);
	NiagaraComp->SetColorParameter(TEXT("Color01"), Color01);
	NiagaraComp->SetColorParameter(TEXT("Color01"), Color02);

	GetWorldTimerManager().SetTimer(TimerHandle_NoPlayAreaEffect, this, &ACPBasePlayer::ReInitiateEffect, NoEffectDuration);
}

// Called every frame
void ACPBasePlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Check if Z CHANGES from Negative to Positive
	
	if (PreviousZ>GetActorLocation().Z && PreviousZ<=51)
	{				
		// Play area effect
		if (!bEffectPlayed)
		{			
			PlayFallingEffect();
			bEffectPlayed = true;
		}				
	}
	
	PreviousZ = GetActorLocation().Z;
}

// Called to bind functionality to input
void ACPBasePlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveUp", this, &ACPBasePlayer::MoveUp);
	InputComponent->BindAxis("MoveRight", this, &ACPBasePlayer::MoveRight);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACPBasePlayer::Jump);	
}


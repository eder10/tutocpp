// Fill out your copyright notice in the Description page of Project Settings.


#include "CWGTBase.h"

#include "TextBlock.h"

UCWGTBase::UCWGTBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UCWGTBase::NativeConstruct()
{
	Super::NativeConstruct();	
}

void UCWGTBase::UpdateScore(int32 Value)
{
	if (TextScore)
	{
		TextScore->SetVisibility(ESlateVisibility::Visible);
	}
	TextScore->SetText(FText::FromString(FString::FromInt(Value)));
}

void UCWGTBase::ResetScore()
{
	if (TextScore)
	{
		TextScore->SetVisibility(ESlateVisibility::Hidden);
		TextScore->SetText(FText::FromString(FString::FromInt(0)));
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "CWGTGameOver.h"
#include "TextBlock.h"

UCWGTGameOver::UCWGTGameOver(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UCWGTGameOver::NativeConstruct()
{
	Super::NativeConstruct();
}

void UCWGTGameOver::UpdateText(FString ValueText)
{
	if (TextGameOver)
	{
		TextGameOver->SetVisibility(ESlateVisibility::Visible);
	}
	TextGameOver->SetText(FText::FromString(ValueText));
}
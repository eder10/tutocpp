// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "BaseCoin.generated.h"

class USoundCue;
class UAudioComponent;

UCLASS()
class COINCOLLECTOR_API ABaseCoin : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseCoin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle DeathTimerHandle;

	UFUNCTION()
	void DeathTimerComplete();

	UFUNCTION()
	void PlaySound();

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue * PropellerAudioCue = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	UAudioComponent * PropellerAudioComponent = nullptr;

	UPROPERTY()
	bool bSoundPlaying =false;

	UPROPERTY()
	bool bIsCollected = false;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* Root = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* CoinMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RotationRate = 100.0f;

	UFUNCTION(BlueprintCallable)
	void PlayCustomDeath(ACPBasePlayer* Player);

	UFUNCTION(BlueprintCallable)
	void CoinRobbed(ACPBasePlayer* AIPlayer);
	
	UFUNCTION(BlueprintNativeEvent)
	void OnOverlap(AActor* OverlappedActor, AActor* OtherActor);
};

// Fill out your copyright notice in the Description page of Project Settings.

#include "CGMBase.h"
#include "CPBasePlayer.h"
#include "BaseCoin.h"

#include "CHUDBase.h"
#include "CoinCollector.h"
#include "CPlayerState.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABaseCoin::ABaseCoin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>("Root");
	RootComponent = Root;
	CoinMesh = CreateDefaultSubobject<UStaticMeshComponent>("CoinMesh");
	CoinMesh->SetupAttachment(Root);
	CoinMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	RotationRate = 100;

	// Setting up overlap
	OnActorBeginOverlap.AddDynamic(this, &ABaseCoin::OnOverlap);

	// Setting up Audio
	PropellerAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("PropellerAudioComp"));
	PropellerAudioComponent->bAutoActivate = false;	
	PropellerAudioComponent->SetupAttachment(Root);
	bSoundPlaying = false;
	
}

// Called when the game starts or when spawned
void ABaseCoin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseCoin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalRotation(FRotator(0, RotationRate * DeltaTime, 0));
}

void ABaseCoin::DeathTimerComplete()
{
	bSoundPlaying = false;
	Destroy();
}

void ABaseCoin::PlaySound()
{
	if (!bSoundPlaying)
	{
		UE_LOG(LogTemp, Warning, TEXT("PlaySound"));
		bSoundPlaying = true;
		if (PropellerAudioComponent->IsValidLowLevelFast()) {
			PropellerAudioComponent->SetSound(PropellerAudioCue);
		}
		PropellerAudioComponent->Play();
	}	
}

void ABaseCoin::PlayCustomDeath(ACPBasePlayer* Player)
{	

	// Manage with gamemode to handle the score with player state
	if (!bIsCollected)
	{
		ACGMBase* GM = Cast<ACGMBase>(GetWorld()->GetAuthGameMode());
		if (GM)
		{
			// Coint collected here and broadcast to handle the function on the blueprint
			GM->OnCoinCollected.Broadcast(GetOwner(), Player);			
			bIsCollected = true;
			
			ACPlayerState* PlayerState = Player->GetController()->GetPlayerState<ACPlayerState>();
			if (PlayerState)
			{				
				GM->CustomGameHud->UpdateScoreText(PlayerState->GetScore());				
			}

			// Check if you got the max score
			if (PlayerState->GetScore()>= COINS_TO_WIN)
			{
				UE_LOG(LogTemp, Warning, TEXT("Finish the game with MAX COINS"));
				GM->FinalizeGame(GAMEOVER_WIN_TEXT);	
			}			
		}
	}
	
	PlaySound();	
	RotationRate = 1500;	
	GetWorldTimerManager().SetTimer(DeathTimerHandle, this, &ABaseCoin::DeathTimerComplete, 1.0f, false);
}

void ABaseCoin::CoinRobbed(ACPBasePlayer* AIPlayer)
{
	// Manage with gamemode to handle the score with player state
	if (!bIsCollected)
	{
		ACGMBase* GM = Cast<ACGMBase>(GetWorld()->GetAuthGameMode());
		if (GM)
		{
			// Coint collected here and broadcast to handle the function on the blueprint
			GM->OnCoinCollected.Broadcast(GetOwner(), AIPlayer);
			bIsCollected = true;

			//TODO: Check if there is no more coins
			GM->CoinsEnemyCollected++;
		}
	}
	
	Destroy();
	
}

void ABaseCoin::OnOverlap_Implementation(AActor* OverlappedActor, AActor* OtherActor)
{	
	if (Cast<ACPBasePlayer>(OtherActor) != nullptr)
	{		
		Destroy();
	}
}



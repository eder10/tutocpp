// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define COINS_TO_WIN	14
#define TIME_TO_LOSE	25
#define GAMEOVER_LOSE_TEXT	"GAME OVER"
#define GAMEOVER_WIN_TEXT	"YOU WIN"